#include<stdio.h>

void merge(long int arr[], long int l, long int m, long int r)
{
    long int i, j, k;
    long int n1 = m - l + 1;
    long int n2 =  r - m;

    long int L[n1], R[n2];

    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];


    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2){
        if (L[i] <= R[j]){
            arr[k] = L[i];
            i++;
        }
        else{
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }
    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}

void merge_dsc(long int arr[], long int l, long int m, long int r)
{
    long int i, j, k;
    long int n1 = m - l + 1;
    long int n2 =  r - m;

    long int L[n1], R[n2];

    for (i = 0; i < n1; i++)
        L[i] = arr[l + i];
    for (j = 0; j < n2; j++)
        R[j] = arr[m + 1+ j];


    i = 0;
    j = 0;
    k = l;
    while (i < n1 && j < n2){
        if (L[i] >= R[j]){
            arr[k] = L[i];
            i++;
        }
        else{
            arr[k] = R[j];
            j++;
        }
        k++;
    }

    while (i < n1){
        arr[k] = L[i];
        i++;
        k++;
    }
    while (j < n2){
        arr[k] = R[j];
        j++;
        k++;
    }
}

void mergeSort(long int arr[], long int l, long int r)
{
    if (l < r)
    {
        long int m = l+(r-l)/2;

        mergeSort(arr, l, m);
        mergeSort(arr, m+1, r);

        merge(arr, l, m, r);
    }
}

void mergeSort_dsc(long int arr[], long int l, long int r)
{
    if (l < r)
    {
        long int m = l+(r-l)/2;

        mergeSort_dsc(arr, l, m);
        mergeSort_dsc(arr, m+1, r);

        merge_dsc(arr, l, m, r);
    }
}

int main()
{
    int T, i;
    scanf("%d", &T);

    for(i=1; i<=T; ++i){
        long int N, j, p, r, P;
        scanf("%ld", &N);
        long int n[N];
        r=N-1;
        p=0;

        for(j=0; j<N; ++j){
            scanf("%ld", &n[j]);
        }
        scanf("%ld", &P);
        if(P==1){
            mergeSort(n, p, r);
            printf("Case %d:\n", i);
            for(j=0; j<N; ++j){
                if(j!=0)
                    printf(" ");
                printf("%ld", n[j]);
            }
            printf("\n");
        }
        else if(P==2){
            mergeSort_dsc(n, p, r);
            printf("Case %d:\n", i);
            for(j=0; j<N; ++j){
                if(j!=0)
                    printf(" ");
                printf("%ld", n[j]);
            }
            printf("\n");
        }
    }
    return 0;
}
